extends Area3D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_Hazard_body_entered(body):
	#body.rpc(&'take_damage', -1, body.global_transform.origin, Vector3.ZERO, 1000, body.global_transform.origin, 0, 0 )
	body.rpc(&'die', body.get_multiplayer_authority())
