extends Control

@onready var editor = $VBoxContainer/Editor
@onready var history = $VBoxContainer/History
@onready var main = owner

enum PromptStatus {UNKNOWN, INVALID, COMMAND, GET, SET}

var commands = [
	'help',
	'quit',
	'coms',
	'vars',
	'clear',
]

const colors = {
	PromptStatus.UNKNOWN : Color.GHOST_WHITE,
	PromptStatus.INVALID : Color.RED,
	PromptStatus.COMMAND : Color.MEDIUM_SPRING_GREEN,
	PromptStatus.GET : Color.PALE_GREEN,
	PromptStatus.SET : Color.DEEP_SKY_BLUE,
}

var prompt_status : PromptStatus:
	set(value):
		prompt_status = value
		
		var prompt_color = colors[prompt_status]
		editor.set('theme_override_colors/font_color', prompt_color)

var prompt
var argument

var prompt_history = []
var prompt_history_index = null

var local_player_input_active_previously : bool

var active := true:
	set(value):
		active = value
		if active:
			#self.show()
			editor.grab_focus()
		else:
			#self.hide()
			editor.release_focus()

# Called when the node enters the scene tree for the first time.
func _ready():
	prompt_status = PromptStatus.UNKNOWN
	command_help(null)
	active = false
	anchor_bottom = 0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if active:
		anchor_bottom = lerp(anchor_bottom, 0.5, delta * 8)
	else:
		anchor_bottom = lerp(anchor_bottom, 0.0, delta * 8)

func toggle_console():
	if Input.is_action_just_pressed(&'console'):
		active = ! active
		if main.local_player:
			if active:
				local_player_input_active_previously = main.local_player.input_active
				main.local_player.input_active = false
			else:
				main.local_player.input_active = local_player_input_active_previously
			
			
		get_tree().get_root().set_input_as_handled()
	#print("Toggling console to ", active)

func _unhandled_input(_event):
	if not active:
		toggle_console()
	
	if Input.is_action_just_pressed(&'clear') and active:
		editor.text = ''
		prompt_status = PromptStatus.UNKNOWN
		get_viewport().set_input_as_handled()

func split_prompt(text:String) -> void:
	prompt = text.get_slice(' ', 0)
	var split = text.split(' ', false, 1)
	
	if split.size() == 2:
		argument = split[1]
	else:
		argument = null

func validate_prompt(text: String):
	# separate command and argument, rest is dropped
	split_prompt(text)
	
	# check if the prompt matches command
	if prompt in commands and has_method(StringName('command_' + prompt)):
		prompt_status = PromptStatus.COMMAND
	# check if we're trying to get a variable
	elif prompt in Settings.settings.keys() and argument == null:
		prompt_status = PromptStatus.GET
	# check if we're trying to set a variable
	elif prompt in Settings.settings.keys() and argument != null:
		prompt_status = PromptStatus.SET
	else:
		prompt_status = PromptStatus.UNKNOWN
		
func _on_editor_text_submitted(new_text):
		
	if prompt_status == PromptStatus.UNKNOWN:
		prompt_status = PromptStatus.INVALID
	
	if prompt_status == PromptStatus.INVALID:
		return
		
	if prompt_status == PromptStatus.COMMAND:
		call(StringName('command_' + prompt), argument)
		
	elif prompt_status == PromptStatus.GET:
		print(prompt)
		print(Settings.get_var(prompt))
		history.newline()
		history.append_text("[color=" + colors[prompt_status].to_html() + "]> " + prompt + " is " + var2str(Settings.get_var(prompt)) + "[/color]")
	
	elif prompt_status == PromptStatus.SET:
		Settings.set_var(prompt, str2var(argument))
		history.newline()
		history.append_text("[color=" + colors[prompt_status].to_html() + "]> " + prompt + " is now " + var2str(Settings.get_var(prompt)) + "[/color]")
	
	
	editor.text = ''
	prompt_status = PromptStatus.UNKNOWN
	

func _on_editor_text_changed(new_text):
	if active:
		toggle_console()
		if not active:
			$VBoxContainer/Editor.text = $VBoxContainer/Editor.text.rstrip('`')
			$VBoxContainer/Editor.caret_column = $VBoxContainer/Editor.text.length()
	validate_prompt(new_text)
	#get_tree().get_root().set_input_as_handled()
	
func command_help(argument):
	
	history.newline()
	history.newline()
	
	if argument == null:
	
		history.append_text(
'''--- Liblast Console Help ---

- Key bindings:

	TILDA toggles the Console
	[s]DELETE clears the current prompt[/s]
	[s]UP/DOWN cycles through prompt history[/s]

- Commands
	
	To view this help text, type "help"
	To list available commands, type "coms"
	To list available variables, type "vars"
	
	To get help for a given command, type "help" and it\'s name

	To get a variable, type it\'s name
	To set a variable, type it\'s name followed by the new value - mind the formatting

- Value formatting:
	
	Strings must be encapsulated in "quotes"
''')
	else:
		match argument:
			'clear' : history.append_text('clear removes all text from the console')
			'quit' : history.append_text('quit closes the game')
		
		
func command_coms(argument) -> void:
	history.newline()
	history.newline()
	history.append_text("Available commands:")
	history.newline()
	history.append_text(var2str(commands))

func command_vars(argument) -> void:
	history.newline()
	history.newline()
	history.append_text("Available variables:")
	history.newline()
	history.append_text(var2str(Settings.settings.keys()))

func command_clear(argument) -> void:
	history.clear()

func command_quit(argument) -> void:
	get_tree().quit()
