extends "res://Assets/Menu/MenuData.gd"

func on_value_changed():
	$LineEdit.text = value

func on_label_changed():
	$Label.text = label

func _on_line_edit_text_submitted(new_text):
	set_var(new_text)
