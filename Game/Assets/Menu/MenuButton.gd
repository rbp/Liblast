extends "res://Assets/Menu/MenuItem.gd"

func on_label_changed():
	self.text = label

func _on_MenuButton_mouse_entered():
	$HoverSound.play()

func _on_MenuButton_pressed():
	$ClickSound.play()
