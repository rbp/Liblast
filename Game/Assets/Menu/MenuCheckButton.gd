extends "res://Assets/Menu/MenuData.gd"

#func set_data(_data):
#	super.set_data(_data)
#	self.pressed = _data

func on_label_changed():
	self.text = label

func on_toggle(button_pressed):
	set_var(button_pressed)
	$ClickSound.play()

func _on_MenuCheckButton_mouse_entered():
	$HoverSound.play()
