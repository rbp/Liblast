extends Node3D

@export var hit_effect_scene : PackedScene
@export var speed : float
var damage : int


var source_position : Vector3
var player
var time := 0.0

#var active := true

@onready var halo = $Halo
@onready var halo_size = halo.mesh.size

var ray_previously : bool = false

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	#$OmniLight3D/Smoke.emitting = true
	pass
	
func _physics_process(delta):
	translate_object_local(Vector3.FORWARD * speed * delta)
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	$Rocket.rotate_z(delta * 4)
	
	time += delta
	
	var flicker = sin(time * 125) + sin(time * 180) / 2 + sin(time * 295) / 3
	
	$OmniLight3D.light_energy = 3 + flicker /2
	halo.mesh.size = halo_size * 1.5 + Vector2(flicker, flicker) / 8
	

func give_damage(target: Node, hit_position: Vector3, hit_normal: Vector3, damage: int, source_position: Vector3, type: Globals.DamageType, push: float):
	if target:
		if target.has_method(&'take_damage'): # we've hit a player or something else - they will handle everything like effects etc.
			target.rpc(&'take_damage',get_multiplayer_authority(), hit_position, hit_normal, damage, source_position, type, push)

		# TODO take data from the material of the target and spawn an appropriate hit effect
#	if target is CharacterBody3D:
#		target.motion_velocity += hit_normal * 10
	
	var hit_effect : Node = hit_effect_scene.instantiate()
	get_tree().root.add_child(hit_effect)
	hit_effect.global_transform.origin = hit_position
	#print(impact_vfx.global_transform)
	var result = hit_effect.look_at(hit_position + hit_normal)
	
	if not result: # if the look_at failed (as it will on floors and ceilings) try another approach:
		hit_effect.look_at(hit_position + hit_normal, Vector3.LEFT)
	
	hit_effect.rotate(hit_normal, randf_range(0, PI * 2))
		
	rpc(&'terminate') # doesn't work!
	
		#print(impact_vfx.global_transform)

@rpc(any_peer, call_local, reliable) func terminate() -> void: # cleanly end the rocket's life
	set_physics_process(false)
	
	halo.hide()
	$OmniLight3D.hide()
	$Smoke.emitting = false
	$Area3D.set_deferred("monitoring",false)
	$Rocket.hide()
	$AmbientSound.stop()
	$Timer2.start()
	

func _on_Area3D_body_entered(body):
		# TODO cast a ray and set position acordingly
	
	var space_state = get_world_3d().direct_space_state
	
	var from = get_global_transform().origin + get_global_transform().basis[2]
	var aim = - get_global_transform().basis[2]
	var to = from + (aim * 4)
	
	var physics_ray_query_parameters_3d = PhysicsRayQueryParameters3D.new()
	physics_ray_query_parameters_3d.from = from
	physics_ray_query_parameters_3d.to = to
	physics_ray_query_parameters_3d.exclude = [self]
	
	var ray = space_state.intersect_ray(physics_ray_query_parameters_3d)
	
	var hit_position
	var hit_normal
	
	if ray:
		global_transform.origin = ray['position'] + ray['normal'] * 1 # offset from the hit surface
		var result = look_at(ray['position'] + ray['normal'] * 10) # orient the explosions towards the hit surface
		if not result: # if the look_at failed, we try a different take
			look_at(ray['position'] + ray['normal'], Vector3.LEFT)
		hit_position = ray['position']
		hit_normal = ray['normal']
	else:
		hit_position = global_transform.origin
		hit_normal = - global_transform.looking_at(body.global_transform.origin).basis[2].normalized() # is this good?
		
	if is_multiplayer_authority(): # only do this on the attacker's local instance of the game
		# direct hit
		give_damage(body, hit_position, hit_normal, 50 , source_position, Globals.DamageType.EXPLOSION, 1)
	else: # everything but the damage on remotes
		give_damage(body, hit_position, hit_normal, 0 , source_position, Globals.DamageType.EXPLOSION, 1)
	
	terminate()

func _on_Timer_timeout():
	queue_free()

func _on_Timer2_timeout():
	queue_free()
