extends RigidDynamicBody3D

@onready var sound = $Sound
@onready var sound_player = sound.get_node("AudioStreamPlayer3D")

@onready @export var smoke_color : Color

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	if Settings.get_var('render_particles_extra'):
		var pmat = $Smoke.process_material.duplicate()
		$Smoke.process_material = pmat
		$Smoke.emitting = true
		$AnimationPlayer.play("SmokeFade")
	else:
		$Smoke.hide()
	
	var mat = $Casing/Casing_LOD0.get_active_material(0).duplicate()
	$Casing/Casing_LOD0.set_surface_override_material(0, mat)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Settings.get_var('render_particles_extra'):
		$Smoke.process_material.color = smoke_color

func _on_Timer_timeout():
	$AnimationPlayer.play("Fade")

func _on_Casing_body_entered(body):
	var vel = linear_velocity.length()
	#print(linear_velocity.length())
	
	if vel > 1:
		sound_player.unit_db = -48 + min((pow(vel, 3)), 48)
		sound.play()
