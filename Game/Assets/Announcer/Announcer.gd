extends AudioStreamPlayer

@onready var go = preload("res://Assets/Announcer/Go.wav")
@onready var defeat = preload("res://Assets/Announcer/Defeat.wav")
@onready var shame = preload("res://Assets/Announcer/Shame.wav")
@onready var victory = preload("res://Assets/Announcer/Victory.wav")
@onready var getready = preload("res://Assets/Announcer/GetReady.wav")
@onready var victory2 = preload("res://Assets/Announcer/MercilessVictory.wav")
@onready var defeat2 = preload("res://Assets/Announcer/EmbarrassingDefeat.wav")
@onready var firstblood = preload("res://Assets/Announcer/FirstBlood.wav")
@onready var yousuck = preload("res://Assets/Announcer/YouSuck.wav")
@onready var payback = preload("res://Assets/Announcer/Payback.wav")
@onready var welcome = preload("res://Assets/Announcer/Welcome.wav")
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func speak(sound):
	stream = sound
	play()
# Called when the node enters the scene tree for the first time.
#func _ready():
#	pass # Replace with function body.
#

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
